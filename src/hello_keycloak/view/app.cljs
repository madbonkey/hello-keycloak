(ns hello-keycloak.view.app
  (:require [hello-keycloak.system.auth :as auth]
            [hello-keycloak.view.components.container :refer [auth-guard]]))



(defn guest-root [{:keys [auth]}]
  [:div
   [:h2 "Please wait ..."]
   [:p
    "If the application doesn't respond, please "
    [:a {:href "/" :on-click #(do (.preventDefault %)
                                  (when-let [keycloak (:instance auth)]
                                    (.login keycloak)))}
     "click here"]
    "."]])

(defn display-access-token []
  (let [token (auth/access-token)]
    (fn []
      [:section
       [:p "Your Keycloak access token is:"]
       [:pre @token]])))


(defn authenticated-root [context user]
  (let [{:keys [email sub]} @user
        {:keys [auth]} context]
    (if-not email
      [:div.no-userinfo
       [:p
        {:style {:color      "rgba(0,0,0,0.75)"
                 :font-style :italic}}
        "No user information present!"]]
      [:div.loaded-userinfo
       [:h2 (str "Good day to you, " email)]
       [:p
        "You're logged in with "
        [:code (str "sub #" sub)]
        ". If you'd like to log out, please "
        [:a {:href "#" :on-click #(do (.preventDefault %)
                                      (when-let [keycloak (:instance auth)]
                                        (.logout keycloak)))}
         "click here"]
        "."]
       [display-access-token]])))


(defn app-root [context]
  [:main.app-root
   [:h1 "Hello, figwheel.main & Keycloak!"]
   [auth-guard
    :context context
    :guest guest-root
    :authenticated authenticated-root]])

