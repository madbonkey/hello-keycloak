(ns hello-keycloak.view.components.container
  (:require [hello-keycloak.system.auth :as auth]))


(defn auth-guard [& _]
  (let [authd? (auth/is-authenticated?)
        user   (auth/userinfo)]
    (fn [& {:keys [guest authenticated context]}]
      (if @authd?
        [authenticated context user]
        [guest context]))))

