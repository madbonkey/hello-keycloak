(ns ^:figwheel-hooks hello-keycloak.core
  (:require [com.stuartsierra.component :as sys]
            [hello-keycloak.config :refer [config]]
            [hello-keycloak.system.auth :refer [new-auth-component]]
            [hello-keycloak.system.ui :refer [new-ui-component]]
            [hello-keycloak.view.app]))


(enable-console-print!)


(defn- client-system []
  (let [{:keys [auth ui]} (config)]
    (sys/map->SystemMap
      {:auth (new-auth-component auth)
       :ui   (-> (new-ui-component ui)
                 (sys/using [:auth]))})))


(defonce ^:private system nil)

(defn start []
  (->> (or system (client-system))
       (sys/start)
       (set! system)))

(defn stop []
  (->> (sys/stop system)
       (set! system)))

(defn reset []
  (js/console.group "App reset")
  (stop)
  (start)
  (js/console.groupEnd))

(defn ^:export go []
  (reset))

(defn ^:after-load on-js-reload []
  (js/console.log "Reloading application")
  (go))

