(ns hello-keycloak.system.ui
  (:require [com.stuartsierra.component :as sys]
            [reagent.core :as r]))



(defn root-node [^String node-id]
  (js/document.getElementById node-id))

(defn render-app [{:as context :keys [root node-id]}]
  (r/render [root context]
            (root-node node-id)))


(defrecord HelloKeycloakUI [root node-id]
  sys/Lifecycle
  (start [this]
    (js/console.info "HelloKeycloakUI: start")
    (->> (render-app this)
         (assoc this :instance)))
  (stop [this]
    (js/console.info "HelloKeycloakUI: stop")
    this))


(defn new-ui-component [{:keys [root node-id]}]
  (->HelloKeycloakUI root ^String node-id))