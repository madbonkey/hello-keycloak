(ns hello-keycloak.system.auth
  (:require [keycloak-js :as Keycloak]
            [com.stuartsierra.component :as sys]
            [re-frame.core :as rf]))


(defn- store-setup [context]
  (rf/reg-event-db
    ::set-authenticated
    (fn [db [_ authd?]]
      (assoc db :authd? authd?)))
  (rf/reg-event-db
    ::set-userinfo
    (fn [db [_ userinfo]]
      (assoc db :userinfo userinfo)))
  (rf/reg-event-db
    ::set-access-token
    (fn [db [_ token]]
      (assoc-in db [:userinfo :access-token] token)))
  (rf/reg-sub
    ::authenticated?
    (fn [db _]
      (:authd? db)))
  (rf/reg-sub
    ::userinfo
    (fn [db _]
      (:userinfo db)))
  (rf/reg-sub
    ::access-token
    (fn [_ _]
      (rf/subscribe [::userinfo]))
    (fn [user _]
      (:access-token user))))


(defn- store-teardown [context]
  (rf/clear-event ::set-authenticated)
  (rf/clear-event ::set-userinfo)
  (rf/clear-event ::set-access-token)
  (rf/clear-sub ::authenticated?)
  (rf/clear-sub ::userinfo)
  (rf/clear-sub ::access-token))


(defn is-authenticated? []
  (rf/subscribe [::authenticated?]))

(defn userinfo []
  (rf/subscribe [::userinfo]))

(defn access-token []
  (rf/subscribe [::access-token]))

(defn- on-load-userinfo-success [instance data]
  (let [token    (.-token instance)
        userinfo (-> (js->clj data :keywordize-keys true)
                     (assoc :access-token token))]
    (js/console.log "Keycloak: received userinfo" userinfo)
    (rf/dispatch [::set-userinfo userinfo])))

(defn- on-load-userinfo-error [instance error]
  (js/console.error error)
  (.logout instance))

(defn- load-userinfo [instance]
  (-> instance
      (.loadUserInfo)
      (.success (partial on-load-userinfo-success instance))
      (.error (partial on-load-userinfo-error instance))))


(defn- on-init-success [instance authd?]
  (js/console.log "Keycloak:" authd? instance)
  (rf/dispatch [::set-authenticated authd?])
  (when authd?
    (load-userinfo instance)))


(defn- on-init-error [instance error]
  (js/console.error "Keycloak error:" error instance)
  (rf/dispatch [::set-authenticated false]))


(defn setup-keycloak [{:keys [init-options client-config]}]
  (js/console.group "Keycloak setup")
  (js/console.log init-options)
  (js/console.log client-config)
  (js/console.groupEnd)
  (let [instance (Keycloak. (clj->js client-config))]
    (-> instance
        (.init (clj->js init-options))
        (.success (partial on-init-success instance))
        (.error (partial on-init-error instance)))
    instance))

(defn- start-token-refresh-interval [instance {:keys [min-validity timeout]}]
  (js/setInterval
    #(-> instance
         (.updateToken min-validity)
         (.then (fn []
                  (when-let [token (and (not= (.-token instance)
                                              @(access-token))
                                        (.-token instance))]
                    (js/console.log "Keycloak: access token was updated")
                    (rf/dispatch-sync [::set-access-token token])
                    (load-userinfo instance)))))
    timeout))

(defn- stop-token-refresh-interval [interval]
  (js/clearInterval interval))

(defn- start-token-refresher [{:as auth :keys [instance refresh-options]}]
  (if-not instance
    auth
    (do
      (js/console.log "Keycloak: Starting auto-refresh for access-token (minValidity=%ds, timeout=%ds)"
                      (:min-validity refresh-options)
                      (:timeout refresh-options))
      (->> (update refresh-options :timeout * 1000) ;; convert timeout to ms
           (start-token-refresh-interval instance)
           (assoc auth :refresher)))))

(defn- stop-token-refresher [{:as auth :keys [refresher]}]
  (when refresher
    (stop-token-refresh-interval refresher))
  (dissoc auth :refresher))

(defrecord HelloKeycloakAuth [init-options client-config refresh-options]
  sys/Lifecycle
  (start [this]
    (js/console.info "HelloKeycloakAuth: start")
    (store-setup this)
    (if-let [instance (:instance this)]
      (do (js/console.info "HelloKeycloakAuth: already set up!")
          (load-userinfo instance)
          (start-token-refresher this))
      (->> (setup-keycloak this)
           (assoc this :instance)
           (start-token-refresher))))
  (stop [this]
    (js/console.info "HelloKeycloakAuth: stop")
    (store-teardown this)
    (stop-token-refresher this)))

(defn new-auth-component [{:keys [init-options client-config refresh-options]}]
  (->HelloKeycloakAuth init-options
                       client-config
                       refresh-options))