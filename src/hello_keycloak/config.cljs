(ns hello-keycloak.config)


(defn auth-config []
  {:client-config
   {:realm       "master",
    :url         "http://localhost:18080/auth",
    :clientId    "test",
    :credentials {:secret "722e0d53-e73b-443c-9833-894b2b71480d"}}
   :init-options
   {:onLoad           "login-required"
    :checkLoginIframe false}
   :refresh-options
   {:min-validity 30
    :timeout      3}})

(defn ui-config []
  {:root    #'hello-keycloak.view.app/app-root
   :node-id "app"})


(defn config []
  {:auth (auth-config)
   :ui   (ui-config)})