# hello-keycloak

Demonstration project for a ClojureScript frontend via `figwheel.main`, and Keycloak (on PostgreSQL) via `docker-compose`.

## System requirements

* Leiningen `>= 2.7.x` (https://leiningen.org/)
* Docker `>= 18.x` ([https://docs.docker.com/](https://docs.docker.com/install/overview/))
* NPM `>= 6.1.x` ([http://docs.npmjs.org/](https://docs.npmjs.com/getting-started/installing-node#install-npm--manage-npm-versions))

## Keycloak/Docker setup

* Run `docker-compose up -d` in the root directory
* Log in to Keycloak at `http://localhost:18080/auth` with
    * User: `keycloakadmin`
    * Password: `keycloakadmin`
* Navigate to "Clients" and create a new one
    * Set _Access Type_ to `confidential`  
    * Set _Root-URL_ to `http://localhost:9500` 
    * Set _Valid Redirect URIs_ to `/`
    * Set _Admin URL_ to `http://localhost:9500`
    * Add a _Web Origin_ from `http://localhost:9500` 
    * *Save the new client!*
* In the client's settings, navigate to "Installation", and select *"Keycloak OIDC JSON"* from Format Options
    * Copy the client's `credentials.secret` value to `src/config.cljs` at the appropriate location
    * Make sure the `:realm` and `:clientId` key in `src/config.cljs` matches the one from the client settings in Keycloak    
* *(optional)* Create a new user in Keycloak to log in with, under "Users", and give it a password under "Credentials"


## Development

_To install NPM dependencies, run_:

    npm install

_To get an interactive development environment run_:

    lein fig:dev

This will auto compile and send all changes to the browser without the
need to reload. After the compilation process is complete, you will
get a Browser Connected REPL. An easy way to try it is:

    (js/alert "Am I connected?")

and you should see an alert in the browser window.

_To clean all compiled files_:

	lein clean

_To create a production build run_:

	lein clean
	lein fig:min

You can check Keycloak's and PostgreSQL's logs with `docker-compose logs -f`.

## License

Copyright © 2018 Nico Schneider

Distributed under the Eclipse Public License either version 1.0 or (at your option) any later version.
