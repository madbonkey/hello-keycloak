(defproject hello-keycloak "0.1.0-SNAPSHOT"
  :description "Demonstration project for ClojureScript, figwheel.main and Keycloak (on PostgreSQL) via docker-compose"
  :url "https://bitbucket.org/madbonkey/hello-keycloak/"
  :license {:name "Eclipse Public License"
            :url  "http://www.eclipse.org/legal/epl-v10.html"}

  :min-lein-version "2.7.1"

  :dependencies [[org.clojure/clojure "1.9.0"]
                 [org.clojure/clojurescript "1.10.312"]
                 [com.stuartsierra/component "0.3.2"]
                 [reagent "0.7.0"]
                 [re-frame "0.10.5"]]

  :source-paths ["src"]

  :aliases {"fig"     ["trampoline" "run" "-m" "figwheel.main"]
            "fig:dev" ["trampoline" "run" "-m" "figwheel.main" "-b" "dev" "-r"]
            "fig:min" ["run" "-m" "figwheel.main" "-O" "advanced" "-bo" "dev"]}

  :repl-options {:port 19999}

  :profiles {:dev {:dependencies   [[com.bhauman/figwheel-main "0.1.1"]
                                    [com.bhauman/rebel-readline-cljs "0.1.3"]]
                   :resource-paths ["resources" "target"]
                   ;; need to add the compliled assets to the :clean-targets
                   :clean-targets  ^{:protect false} ["target/public" :target-path]
                   :compiler {:install-deps true
                              :infer-externs true
                              :npm-deps {:keycloak-js "4.0.0"}}}})
